package main

import (
	"fmt"
	"go-example/utils"

	"github.com/gin-gonic/gin"
)

type Nick struct {
	Value string `json:"value"`
	Sum   int    `json:"sum"`
}

func init() {
	fmt.Println("init go-example")
}

func main() {

	r := gin.Default()
	r.POST("/ping", func(c *gin.Context) {
		utils.RespondWithJSON(c, 200, "success", "message", nil)
	})

	r.GET("/pang", func(c *gin.Context) {
		utils.RespondWithJSON(c, 200, "success", "message", Nick{
			Value: "nick",
			Sum:   100,
		})
	})

	r.Run()
}
